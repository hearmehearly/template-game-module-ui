﻿namespace TemplateUi
{
    public enum ScreenType
    {
        None                    = 0,
        MainMenu                = 1,
        Ingame                  = 2,
        Result                  = 3,

        Settings                = 5,
        Tutorial                = 6,
        SkinScreen              = 8,
        OkayScreen              = 9,
        RateUsScreen            = 10,
        Roulette                = 11,
        ShopResult              = 12,
        PauseScreen             = 13,
        WeaponSkinScreen        = 14,
        LoadingScreen           = 15,
        RateUsFeedbackScreen    = 16,
        SubscriptionScreenRoot  = 17,
        SubscriptionRewardRoot  = 18,
        SpinRoulette            = 19,
        Rewards              = 20,
        ForceMeter              = 21,
        BirdHunt                = 22,
        BonusLevelPropose       = 23,
        ShopMenu                = 24,
        Mansion                 = 25,
        CurrencyPropose         = 26,
        PremiumShopResult       = 27,


        TileBubble = 28,
        Dialog = 29,
        Chests = 30,
        TileBubbleResourceExtraction = 31,
        UiResourceExtractionOperations = 32,

        UiConfirmScreen = 33,
        MainBase = 34,
        OperationInvestigation = 35,
        OperationBlackHole = 36,
    }
}
