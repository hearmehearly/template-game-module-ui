﻿using UnityEngine;


namespace TemplateUi.Ui
{
    public class UiCamera : SingletonMonoBehaviour<UiCamera>
    {
        [SerializeField] private Camera uiCamera = default;


        public Camera Camera => uiCamera;
    }
}
