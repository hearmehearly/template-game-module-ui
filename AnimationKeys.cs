﻿namespace TemplateUi.Ui
{
    public static class AnimationKeys
    {
        public static class Screen
        {
            public const string Show = "Show";
            public const string Hide = "Hide";
        }

        public static class States
        {
            public const string Common = "Common";
            public const string Ads = "Ads";
        }

        public static class Common
        {
            public const string Open = "Open";
        }
    }
}
