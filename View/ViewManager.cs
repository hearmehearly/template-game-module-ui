﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace TemplateUi.Ui
{
    public static class ViewManager
    {
        #region Fields

        public static Action OnViewInfosChanged;

        const float ZOffset = 100f;
        const int OrderOffset = 10;

        const int DirectionMultiplier = -1;

        const int AdditionalSortingOrderValueForOverlay = 1;
        const int AdditionalZDistanceForOverlay = 1;

        static readonly List<AnimatorView> views = new List<AnimatorView>();

        #endregion



        #region Properties

        public static int SortingOrderForOverlay { get; private set; }

        public static float ZDistanceForOverlay { get; private set; }

        #endregion



        #region Methods

        public static void AddViewInfo(AnimatorView viewInfo)
        {
            views.Add(viewInfo);

            RecalculateViewInfos();            
        }


        public static void RemoveLastViewInfo(AnimatorView animatorView)
        {
            bool isContain = views.Contains(animatorView);

            if (isContain)
            {
                views.Remove(animatorView);
            }

            RecalculateViewInfos();
        }


        static float CalculateZPosition(int index) => ZOffset * index * DirectionMultiplier;


        static int CalculateSortingOrder(int itemIndex) => OrderOffset * itemIndex;


        static void RecalculateViewInfos()
        {
            // TODO: think about it
            views.RemoveAll(view => view == null);
                        
            for (int i = 0; i < views.Count; i++)
            {
                int order = i + 1;

                views[i].SortingOrder = CalculateSortingOrder(order);
                views[i].ZPosition = CalculateZPosition(order);

                views[i].SetVisualOrderSettings();
            }

            if (views.Count > 0)
            {
                SortingOrderForOverlay = views.Max(element => element.SortingOrder) + AdditionalSortingOrderValueForOverlay;
                ZDistanceForOverlay = views.Min(element => element.ZPosition) + (AdditionalZDistanceForOverlay * DirectionMultiplier);
            }
        }

        #endregion
    }
}
