﻿namespace TemplateUi
{
    public enum DialogPopupType
    {
        None = 0,
        NoInternet = 1,
        PurchaseFail = 2,
        PurchaseRestoreOk = 3,
        PurchaseRestoreFail = 4,
        TermAndPrivactWrong = 5,
        VideoNotAvailable = 6
    }
}
