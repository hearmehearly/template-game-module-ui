﻿namespace TemplateUi
{
    public enum PopupType
    {
        None            = 0,
        SimpleDialog    = 1,
        LevelResult     = 2,
        Tutorial        = 3
    }
}
