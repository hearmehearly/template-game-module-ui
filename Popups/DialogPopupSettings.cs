﻿using UnityEngine;
using System;

namespace TemplateUi.Ui
{
    [CreateAssetMenu(fileName = "DialogPopupSettings",
                     menuName = NamingUtility.MenuItems.GuiSettings + "DialogPopupSettings")]
    public class DialogPopupSettings : ScriptableObject
    {
        #region Helpers

        [Serializable]
        public class DialogSettingsContainer
        {
            public OkPopupType dialogPopupType = default;
            public string headerText = default;
            public string contentText = default;
        }

        #endregion



        #region Fields

        [SerializeField] private DialogSettingsContainer[] dialogSettings = default;

        #endregion



        #region Methods

        public DialogSettingsContainer GetSettings(OkPopupType type)
        {
            return  Array.Find(dialogSettings, (item) => item.dialogPopupType == type);
        }

        #endregion
    }
}
