﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace TemplateUi.Ui
{
    public abstract class BaseUiScreenManager : SingletonMonoBehaviour<BaseUiScreenManager>
    {
        private readonly List<AnimatorScreen> activeScreens = new List<AnimatorScreen>();



        public AnimatorScreen ShowScreen(ScreenType type,
                                         Action<AnimatorView> onShowed = null,
                                         Action<AnimatorView> onHided = null,
                                         Action<AnimatorView> onShowBegin = null,
                                         Action<AnimatorView> onHideBegin = null,
                                         Action<AnimatorView> dataSetCallback = null,
                                         object data = default,
                                         bool isForceHideIfExist = true)
        {
            bool isScreenExist = IsScreenShown(type);

            if (isScreenExist)
            {
                if (isForceHideIfExist)
                {
                    AnimatorScreen existScreen = LoadedScreen<AnimatorScreen>(type);

                    if (existScreen != null)
                    {
                        existScreen.HideImmediately();
                    }
                }
                else
                {
                    Debug.Log($"Trying to show already active screen with type {type}");

                    return null;
                }
            }

            GameObject prefabToShow = ScreenPrefabByType(type);

            if (prefabToShow == null)
            {
                Debug.Log($"Screen prefab missing for type {type}");

                return null;
            }

            AnimatorScreen screenToShow = Instantiate(prefabToShow).GetComponent<AnimatorScreen>();

           screenToShow.gameObject.SetActive(false);

            activeScreens.Add(screenToShow);

            onHided += hidedView =>
                {
                    ViewManager.RemoveLastViewInfo(hidedView);

                    RemoveScreenFromActiveList(screenToShow);
                };

            screenToShow.SetDataSetCallback(dataSetCallback);
            screenToShow.SetShowBeginCallback(onShowBegin);
            screenToShow.SetShowEndCallback(onShowed);

            screenToShow.SetHideBeginCallback(onHideBegin);
            screenToShow.SetHideEndCallback(onHided);

            screenToShow.SetData(data);
            screenToShow.Initialize();

            ViewManager.AddViewInfo(screenToShow);

            screenToShow.Show();

            return screenToShow;
        }


        public void HideScreen(ScreenType type, Action<AnimatorView> callback = null)
        {
            var foundScreen = LoadedScreen<AnimatorScreen>(type);

            if (foundScreen != null)
            {
                foundScreen.Hide(callback, null);
            }
        }


        public void HideScreenImmediately(ScreenType type, Action<AnimatorView> callback = null)
        {
            var foundScreen = LoadedScreen<AnimatorScreen>(type);

            if (foundScreen != null)
            {
                foundScreen.HideImmediately(callback);
            }
        }


        public bool IsScreenShown(ScreenType type) => LoadedScreen<AnimatorScreen>(type) != null;


        public T LoadedScreen<T>(ScreenType type) where T : AnimatorScreen
        {
            AnimatorScreen result = activeScreens.Find(element => element.ScreenType == type);

            return (result as T);
        }

        public T LoadedScreen<T>() where T : AnimatorScreen
        {
            AnimatorScreen result = activeScreens.Find(element => element is T);

            return (result as T);
        }

        protected abstract GameObject ScreenPrefabByType(ScreenType type);


        private void RemoveScreenFromActiveList(AnimatorScreen screen)
        {
            if (activeScreens.Count > 0)
            {
                activeScreens.Remove(screen);
                screen.Deinitialize();
                Destroy(screen.gameObject);
            }
        }
    }
}