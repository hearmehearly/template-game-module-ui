﻿using Core;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace TemplateUi.Ui
{
    [RequireComponent(typeof(Animator))]
    public abstract class AnimatorView : MonoBehaviour, IView
    {
        public event Action<AnimatorView> OnHideEnd;
        public event Action<AnimatorView> OnHideBegin;

        public event Action<AnimatorView> OnShowBegin;
        public event Action<AnimatorView> OnShowEnd;
        
        protected Animator mainAnimator;
        protected Canvas mainCanvas;

        private List<ShowState> showStates;
        private List<HideState> hideStates;

        protected Action<AnimatorView> dataSetCallback;

        protected Action<AnimatorView> showEndCallback;
        protected Action<AnimatorView> hideEndCallback;

        protected Action<AnimatorView> showBeginCallback;
        protected Action<AnimatorView> hideBeginCallback;

        private RectTransform rect;
        protected object data;



        protected abstract string ShowKey { get; }

        protected abstract string HideKey { get; }

        protected virtual string IdleBeforeHideKey { get; }

        protected virtual string IdleAfterShowKey { get; }

        protected RectTransform Rect
        {
            get
            {
                if (rect == null)
                {
                    rect = GetComponent<RectTransform>();
                }

                return rect;
            }
        }



        public abstract ViewType Type { get; }

        public int SortingOrder { get; set; }

        public float ZPosition { get; set; }

        public abstract void SetVisualOrderSettings();


        public virtual void Show(Action<AnimatorView> onShowEnd, Action<AnimatorView> onShowBegin)
        {
            showEndCallback += onShowEnd;
            showBeginCallback += onShowBegin;

            Show();
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
            SubscribeEvents();

            OnShowBegin?.Invoke(this);

            mainAnimator.enabled = true;
            SetInputStateEnabled(false);

            mainAnimator.SetTrigger(ShowKey);
        }


        public void Hide(Action<AnimatorView> onHideEnd, Action<AnimatorView> onHideBegin)
        {
            hideEndCallback += onHideEnd;
            hideBeginCallback += onHideBegin;

            Hide();
        }


        public virtual void Hide()
        {
            OnHideBegin?.Invoke(this);

            SetInputStateEnabled(false);

            mainAnimator.SetTrigger(HideKey);

            if (!string.IsNullOrEmpty(IdleBeforeHideKey))
            {
                mainAnimator.Play(IdleBeforeHideKey);
            }
        }


        public void HideImmediately(Action<AnimatorView> callback)
        {
            hideEndCallback += callback;

            HideImmediately();
        }


        public void HideImmediately()
        {
            OnHideBegin?.Invoke(this);

            FinishHide();
        }


        private void FinishHide()
        {
            SetInputStateEnabled(true);

            UnsubscribeEvents();

            gameObject.SetActive(false);

            OnHideEnd?.Invoke(this);

            hideEndCallback?.Invoke(this);
            hideEndCallback = null;
        }

        public virtual void SetData(object dataToSet)
        {
            data = dataToSet;
            dataSetCallback?.Invoke(this);
        }

        public virtual void Initialize()
        {
            gameObject.SetActive(false);

            mainAnimator = GetComponent<Animator>();
            mainAnimator.enabled = false;

            mainCanvas = mainCanvas ?? GetComponent<Canvas>();
            mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            mainCanvas.worldCamera = UiCamera.Instance.Camera;

            InitPosition();
        }


        public virtual void Deinitialize()
        {
            UnsubscribeEvents();
        }
        public virtual void SetDataSetCallback(Action<AnimatorView> callback) =>
            dataSetCallback = callback;

        public void SetShowBeginCallback(Action<AnimatorView> callback) =>
            showBeginCallback = callback;

        public void SetShowEndCallback(Action<AnimatorView> callback) =>
            showEndCallback = callback;

        public void SetHideBeginCallback(Action<AnimatorView> callback) =>
            hideBeginCallback = callback;


        public void SetHideEndCallback(Action<AnimatorView> callback) =>
            hideEndCallback = callback;

        private void SubscribeEvents()
        {
            showStates = new List<ShowState>(mainAnimator.GetBehaviours<ShowState>());
            showStates.ForEach(state =>
            {
                state.Initialize(mainAnimator);
                state.OnShowBegin += ShowState_OnShowBegin;
                state.OnShowEnd += ShowState_OnShowEnd;
            });


            hideStates = new List<HideState>(mainAnimator.GetBehaviours<HideState>());
            hideStates.ForEach(state =>
            {
                state.Initialize(mainAnimator);
                state.OnHideBegin += HideState_OnHideBegin;
                state.OnHideEnd += HideState_OnHideEnd;
            });
        }


        private void UnsubscribeEvents()
        {
            showStates.ForEach(state =>
            {
                state.OnShowBegin -= ShowState_OnShowBegin;
            });

            hideStates.ForEach(state =>
            {
                state.OnHideBegin -= HideState_OnHideBegin;
                state.OnHideEnd -= HideState_OnHideEnd;
            });
        }


        protected abstract void InitPosition();


        private void FinishShow()
        {
            SetInputStateEnabled(true);

            OnShowEnd?.Invoke(this);

            showEndCallback?.Invoke(this);
            showEndCallback = null;
        }


        protected void SetStrigger(string key) => mainAnimator.SetTrigger(key);


        protected void SetInputStateEnabled(bool isEnabled) =>
            EventSystemController.SetSystemEnabled(isEnabled, this);
        

        private void ShowState_OnShowBegin()
        { 
            showBeginCallback?.Invoke(this);
            showBeginCallback = null;
        }


        private void ShowState_OnShowEnd()
        {
            FinishShow();
        }


        private void HideState_OnHideBegin()
        {
            hideBeginCallback?.Invoke(this);
            hideBeginCallback = null;
        }


        private void HideState_OnHideEnd()
        {
            FinishHide();
        }
    }
}
