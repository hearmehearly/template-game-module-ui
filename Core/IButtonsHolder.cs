﻿namespace TemplateUi.Ui.Interfaces
{
    public interface IButtonsHolder
    {
        void InitializeButtons();
        void DeinitializeButtons();
    }
}
