﻿using TemplateUi.Ui.Interfaces;
using UnityEngine;


namespace TemplateUi.Ui
{    
    public abstract class AnimatorScreen : AnimatorView, IButtonsHolder
    {
        public abstract ScreenType ScreenType { get; }
        
        public override ViewType Type => ViewType.Screen;

        protected override string ShowKey => AnimationKeys.Screen.Show;

        protected override string HideKey => AnimationKeys.Screen.Hide;

        public override void Show()
        {
            base.Show();

            InitializeButtons();
        }


        public override void Hide()
        {
            DeinitializeButtons();

            base.Hide();
        }

        public abstract void DeinitializeButtons();
        public abstract void InitializeButtons();


        public override void SetVisualOrderSettings()
        {
            transform.position = transform.position.SetZ(ZPosition);
            mainCanvas.sortingOrder = SortingOrder;
        }


        protected override void InitPosition()
        {
            transform.localScale = Vector3.one;

            Rect.offsetMin = Vector2.zero;
            Rect.offsetMax = Vector2.zero;
        }
    }
}
